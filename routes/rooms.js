const router = require("express").Router();
const Room = require("../models/room");
const Message = require("../models/message")

router.get("/", async (req, res) => {
    try {
        const rooms = await Room.find({})
        res.status(200).json(rooms);
    } catch (err) {
        console.log(err)
        res.status(500).json(err);
    }
});

router.get("/:roomId", async (req, res) => {
    try {
        const room = await Room.findById(req.params.roomId)
        res.status(200).json(room);
    } catch (err) {
        console.log(err)
        res.status(500).json(err);
    }
});

router.post("/", async (req, res) => {
    const newRoom = new Room({
        title: req.body.title,
        members: [req.body.creatorName],
    });
    try {
        const savedRoom = await newRoom.save();
        res.status(200).json(savedRoom);
    } catch (err) {
        res.status(500).json(err);
    }
});

router.put("/:roomId/join", async (req, res) => {
    try {
        const room = await Room.findById(req.params.roomId);
        if (room) {
          await room.updateOne({ $push: { members: req.body.displayName } });
          res.status(200).json("join success");
        } else {
          res.status(403).json("join fail");
        }
      } catch (err) {
        res.status(500).json(err);
      }
});

router.put("/:roomId/leave", async (req, res) => {
    try {
        const room = await Room.findById(req.params.roomId);
        if (room) {
          await room.updateOne({ $pull: { members: req.body.displayName } })
        } else {
          res.status(403).json("leave fail");
        }
        if(room && room.members.length === 1){
          await room.deleteOne();
          await Message.deleteMany({
            roomId: req.params.roomId}
          );
        }
        res.status(200).json("leave success");
      } catch (err) {
        console.log(err)
        res.status(500).json(err);
      }
});

module.exports = router;