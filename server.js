const express = require("express");
const app = express();
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const roomRoute = require("./routes/rooms");
const messageRoute = require("./routes/messages");
const cors = require('cors');

dotenv.config();


mongoose.connect(
  process.env.MONGO_URL,
  { useNewUrlParser: true, useUnifiedTopology: true },
  () => {
    console.log("Connected to MongoDB");
  }
);

const corsOptions = {
    origin: true,
    credentials: true,
}
app.use(cors(corsOptions))

//middleware
app.use(express.json());

app.use("/api/room", roomRoute);
app.use("/api/message", messageRoute);

app.listen(9000, () => {
  console.log("Backend server is running!");
});